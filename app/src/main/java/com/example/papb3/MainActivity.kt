package com.example.papb3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.isInvisible
import androidx.core.view.isVisible


class MainActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val imgkumis = findViewById<ImageView>(R.id.imgkumis)
        val imgalis = findViewById<ImageView>(R.id.imgalis)
        val imgjanggut = findViewById<ImageView>(R.id.imgjanggut)
        val imgrambut = findViewById<ImageView>(R.id.imgrambut)

        val cekkumis = findViewById<CheckBox>(R.id.cekkumis)
        val cekalis = findViewById<CheckBox>(R.id.cekalis)
        val cekjanggut = findViewById<CheckBox>(R.id.cekjanggut)
        val cekrambut = findViewById<CheckBox>(R.id.cekrambut)

        //Kumis Listener
        cekkumis.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                imgkumis.visibility = View.VISIBLE
            } else {
                imgkumis.visibility = View.INVISIBLE
            }
        }

        //Alis Listener
        cekalis.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                imgalis.visibility = View.VISIBLE
                //jika kondisi Checkbox tercentang maka visibility set to VISIBLE
            } else {
                imgalis.visibility = View.INVISIBLE
            }
        }

        //Rambut Listener
        cekrambut.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                imgrambut.visibility = View.VISIBLE
            } else {
                imgrambut.visibility = View.INVISIBLE
            }
        }

        //Janggut Listener
        cekjanggut.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                imgjanggut.visibility = View.VISIBLE
            } else {
                imgjanggut.visibility = View.INVISIBLE
            }
        }

    }

}

